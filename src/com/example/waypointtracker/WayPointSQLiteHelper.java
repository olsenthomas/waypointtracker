package com.example.waypointtracker;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.util.Log;


public class WayPointSQLiteHelper extends SQLiteOpenHelper {
	
	  public static final String TABLE_NAME = "waypoints";
	  public static final String COLUMN_ID = "_id";
	  public static final String COLUMN_LONGITUDE = "longitude";
	  public static final String COLUMN_LATITUDE  = "latitude";
	  public static final String COLUMN_RECORDED  = "recorded";
	  

	  private static final String DATABASE_NAME = "waypointtracker.db";
	  private static final int DATABASE_VERSION = 1;

	  // Database creation sql statement
	  private static final String DATABASE_CREATE = "create table "
	      + TABLE_NAME + "(" + COLUMN_ID + " integer primary key autoincrement, " 
		  + COLUMN_RECORDED  + " NUMERIC not null,"
		  + COLUMN_LONGITUDE  + " REAL not null,"
		  + COLUMN_LATITUDE  + " REAL not null"
		  + ");";

	  public WayPointSQLiteHelper(Context context) {		 
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	    Log.d(WayPointSQLiteHelper.class.getName(), DATABASE_CREATE);
	  }

	  @Override
	  public void onCreate(SQLiteDatabase database) {
	    database.execSQL(DATABASE_CREATE);
	  }

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    Log.w(WayPointSQLiteHelper.class.getName(),
	        "Upgrading database from version " + oldVersion + " to "
	            + newVersion + ", which will destroy all old data");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
	    onCreate(db);
	  }

}
