package com.example.waypointtracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;

// http://www.vogella.com/articles/AndroidSQLite/article.html

public class WayPointDatasource {
	
	// Database fields
	  private SQLiteDatabase database = null;
	  private WayPointSQLiteHelper dbHelper = null;
	  
	  public WayPointDatasource(Context context) {
	    dbHelper = new WayPointSQLiteHelper(context);
	  }

	  public void open() throws SQLException {
	    database = dbHelper.getWritableDatabase();
	  }

	  public void close() {
	    dbHelper.close();
	  }

	  public long createWayPoint(Location location) {
	    ContentValues values = new ContentValues();
	    values.put(WayPointSQLiteHelper.COLUMN_LATITUDE, location.getLatitude());
	    values.put(WayPointSQLiteHelper.COLUMN_LONGITUDE, location.getLongitude());
	    values.put(WayPointSQLiteHelper.COLUMN_RECORDED, location.getTime());
	    long insertId = database.insert(WayPointSQLiteHelper.TABLE_NAME, null, values);
        return insertId;
	  }	 
	  
	  public void ClearWayPoints()
	  {
		  database.execSQL("Delete from " + WayPointSQLiteHelper.TABLE_NAME + ";");
	  }
	  
	  public String GetWayPoints()
	  {
		  Cursor c = database.query(WayPointSQLiteHelper.TABLE_NAME, new String[] {
				    WayPointSQLiteHelper.COLUMN_ID, 
				    WayPointSQLiteHelper.COLUMN_RECORDED,
				    WayPointSQLiteHelper.COLUMN_LONGITUDE,
				    WayPointSQLiteHelper.COLUMN_LATITUDE}, 
	                null, 
	                null, 
	                null, 
	                null, 
	                null);
		  
		  String text = "";
		  if (c.moveToFirst())
		  {
		    do { 
		    	
		    	 if (text != "")
		    		 text += ", ";
		    	 
		         text += "{" +
		        		 "\"rec\"  : " + c.getString(1) + "," +
		        		 "\"long\" : " + c.getString(2) + "," +
		        		 "\"lat\"  : " + c.getString(3) + "}";
		        		 
		       } while (c.moveToNext());
		   }
		     
	      return "{ \"wp\": [" + text + "]}";		  
	  }


}
