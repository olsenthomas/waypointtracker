package com.example.waypointtracker;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;

import com.example.waypointtracker.R;
import com.example.waypointtracker.MyLocationListener;
import com.example.waypointtracker.LocationService.MyBinder;

import android.os.*;
import android.provider.Settings;
import android.util.Log;
import android.app.Activity;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.view.View;
import android.view.View.OnClickListener;
import android.location.*;
import android.content.*;
import android.os.IBinder;


public class MainActivity extends Activity {
	private Context ac;
	private LocationService mService;
    private boolean mBound = false;
    private LocationManager manager = null;
    private WayPointDatasource datasource = null;
	
    static public final String logTag = "WayPointTracker"; 
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ac = this;
        
        if (datasource == null)
		{
			datasource = new WayPointDatasource(this);
			datasource.open();
		}
        
        Button buttonStart = (Button) findViewById(R.id.button1);
        buttonStart.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
            	EditText editText = (EditText) findViewById(R.id.editText1);
            	editText.setText("Started the app\n");
            	Log.d(MainActivity.logTag, "Started the app");
            	try
            	{
                  //http://stackoverflow.com/questions/3470693/android-get-location-or-prompt-to-enable-location-service-if-disabled		
            	  //http://stackoverflow.com/questions/2279647/how-to-emulate-gps-location-in-the-android-emulator
            	  //http://stackoverflow.com/questions/1608632/android-locationmanager-getlastknownlocation-returns-null
            	  //http://developer.android.com/training/basics/location/currentlocation.html	
            	  if (manager == null)
            	  { 
            		  
	            	  manager = (LocationManager) getSystemService(LOCATION_SERVICE);
	            	  
	            	  
	            	  Criteria locationCritera = new Criteria();
	                  locationCritera.setAccuracy(Criteria.ACCURACY_FINE);
	                  locationCritera.setAltitudeRequired(false);
	                  locationCritera.setBearingRequired(false);
	                  locationCritera.setCostAllowed(true);
	                  locationCritera.setPowerRequirement(Criteria.NO_REQUIREMENT);
	                  
	                  String providerName = manager.getBestProvider(locationCritera, true);
	            	  
	                  if (providerName != null && manager.isProviderEnabled(providerName)) {
	                      // Provider is enabled
	                	  if (mBound)
	                	  {
	                		  //manager.requestLocationUpdates(providerName, 5000, 10, new MyLocationListener(editText));
	                		  mService.StartLocationListener(manager, providerName, editText, ac, datasource);
	                	  }
	                	  
	                      
	                  } else {
	                      // Provider not enabled, prompt user to enable it
	                      Toast.makeText(ac, R.string.gps, Toast.LENGTH_LONG).show();
	                      Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	                      startActivity(myIntent);
	                  }
	                  
	                  Log.d(MainActivity.logTag, "Provider " + providerName + " has been selected.");
	                  //Location location = manager.getLastKnownLocation(providerName); 
            	  }
            	  
            	}
            	catch (Exception e)
            	{  
            		editText.append(e.toString() + "\n");
            		Log.e(MainActivity.logTag, e.toString());
            	}
            }
        });
        
        Button buttonStop = (Button) findViewById(R.id.button2);
        buttonStop.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
            	if (mBound && (manager != null))
            	{
            		mService.StopLocationListener(manager);            		
            	}
            	EditText editText = (EditText) findViewById(R.id.editText1);
            	Log.d(MainActivity.logTag, "Stopped the app");                
            	editText.setText("Stopped the app\n");
            	manager = null;
            }
        });
        
        Button buttonUpload = (Button) findViewById(R.id.button3);
        buttonUpload.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
            	
            	if (mBound)
            	{
            		if (manager != null)
            		{
            		  mService.StopLocationListener(manager);
            		  manager = null;	  
            		}
            		            		
            	
            	
	            	EditText editText = (EditText) findViewById(R.id.editText1);
	            	editText.setText("Uploading data ...... (But not really yet!!! Just a test button)\n");
	            	Log.d(MainActivity.logTag, "Uploading data ...... (But not really yet!!! Just a test button)");
	            	
	            	if (datasource != null )
	            	{
	            		editText.append(datasource.GetWayPoints());
	            		new Thread(new UploadWayPoints(datasource.GetWayPoints())).start();
	            		datasource.ClearWayPoints();
	            		
	            	}
            	
            	}	
            	            	         
            	
            }
        });
       
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, LocationService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    
    
    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MyBinder binder = (MyBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            Log.d(MainActivity.logTag, "ServiceConnection: onServiceDisconnected");
        }
    };
    
    class UploadWayPoints implements Runnable
    {
    	private Exception e = null;
    	private String data = "";

    	public UploadWayPoints(String data)
    	{
    		this.data = data;
    	}
    	
    	private void readStream(InputStream in) {
    		  BufferedReader reader = null;
    		  try {
    		    reader = new BufferedReader(new InputStreamReader(in));
    		    String line = "";
    		    while ((line = reader.readLine()) != null) {
    		      Log.d(MainActivity.logTag, line);	
    		    }
    		  } catch (IOException e) {
    			Log.e(MainActivity.logTag, e.getMessage());	  
    		    e.printStackTrace();
    		  } finally {
    		    if (reader != null) {
    		      try {
    		        reader.close();
    		      } catch (IOException e) {
    		    	  Log.e(MainActivity.logTag, e.getMessage());
    		        e.printStackTrace();
    		        }
    		    }
    		  }
    		} 
    	
		public void run() {
			
			// http://www.vogella.com/articles/AndroidNetworking/article.html
			
			
			Socket socket;
			try {
				URL url = new URL("http://www.vogella.com");
				  HttpURLConnection con = (HttpURLConnection) url.openConnection();
				  readStream(con.getInputStream());
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				Log.e(MainActivity.logTag, e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e(MainActivity.logTag, e.getMessage());
			} catch (Exception e) {
			// TODO Auto-generated catch block
			   e.printStackTrace();
			   Log.e(MainActivity.logTag, e.getMessage());
		    }  	
			
		}
    	
    }
    
}
