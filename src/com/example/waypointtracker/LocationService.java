package com.example.waypointtracker;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import com.example.waypointtracker.MyLocationListener;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.IBinder;
import android.os.Binder;
import android.provider.Settings;
import android.widget.*;


public class LocationService extends Service {
	
	private final IBinder mBinder = new MyBinder();
	private MyLocationListener listener = null;
	
	
	public LocationService()
	{
		
	}
	
	public void StartLocationListener(LocationManager manager, String providerName, EditText editText, Context context, WayPointDatasource datasource)
	{
		if (listener == null)
		{
		  listener = new MyLocationListener(editText, context, datasource);
		  manager.requestLocationUpdates(providerName, 10000, 100, listener);
		}
	}
	
	public void StopLocationListener(LocationManager manager)
	{
		
		if (listener != null)
		{
		  manager.removeUpdates(listener);
		  listener = null;
		}
	}	
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return mBinder;
	}
	
	
	public class MyBinder extends Binder {
	    LocationService getService() {
	      return LocationService.this;
	    }
	  }

}
