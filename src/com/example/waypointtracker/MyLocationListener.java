package com.example.waypointtracker;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class MyLocationListener implements LocationListener {
	
	private EditText textArea = null;
	private Context context = null;
	private WayPointDatasource datasource = null;
	
	public MyLocationListener(EditText text, Context context, WayPointDatasource datasource)
	{
		this.textArea = text;
		this.context = context;
		this.datasource = datasource;
	}
	

	public void onLocationChanged(Location location) {
		
		datasource.createWayPoint(location);
		
		if (textArea.getLineCount() > 8)
			textArea.setText("");
			
		textArea.append(getDate(location.getTime(), "dd/MM/yyyy hh:mm:ss") + "\n     " + location.getProvider() + " Way point recorded\n");
		
		Log.d(MainActivity.logTag, "onLocationChanged");
		Log.d(MainActivity.logTag, location.getProvider() + ";" + location.getLongitude() + ";" + location.getLatitude() + ";" + location.getTime());

	} 

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		Log.d(MainActivity.logTag, "onProviderDisabled");

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		Log.d(MainActivity.logTag, "onProviderEnabled");

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		Log.d(MainActivity.logTag, "onStatusChanged");
	}
	
	public String getDate(long milliSeconds, String dateFormat)
	{
	    // Create a DateFormatter object for displaying date in specified format.
	    DateFormat formatter = new SimpleDateFormat(dateFormat);

	    // Create a calendar object that will convert the date and time value in milliseconds to date. 
	     Calendar calendar = Calendar.getInstance();
	     calendar.setTimeInMillis(milliSeconds);
	     return formatter.format(calendar.getTime());
	}

}
